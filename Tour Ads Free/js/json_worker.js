// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+
const URL_FOR_GET_TOUR_ADS = "http://localhost:8080/free/rest/api/tour";
const URL_FOR_GET_COUNTRY_NAMES = "http://localhost:8080/free/rest/api/countries";

// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+




// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+


async function getTourAdsRequest(url) {
    var TOUR_ADS = [];
    await fetch(url, {
      })
      .then((res) => res.json())
      .then((tourAds) => {
        TOUR_ADS= TOUR_ADS.concat(tourAds);
      }
    
    )
    return TOUR_ADS;
}
const TOUR_ADS_CONTAINER_LIST = document.getElementById('tour-ads-container-list');
var TOUR_ADS_LIST = [];


function getStart(arrayStars){
    var stringStarts = "";
    arrayStars.forEach(element => stringStarts += element);
    return stringStarts;
}

function getTourAdDivs(tourAds){
    var stringDivs = "";
    tourAds.forEach(element => stringDivs += getTourAdHTML(element));
    return stringDivs;
}

function getTourAdHTML(tourAd){
    return `<div class="tour-ad-block div-flex-row">
    <div id="name-and-date-work-and-place-and-country-and-city" class="div-flex-column-base"
        style="height: 160px; ">

        <p class="travel-agency-name-box item_name-travel-agency">${tourAd.nameAgency}</p>
        <div class="div-flex-row-between">
            <div class="date-work-and-country-and-city color-box-tour-ad">
                <p class="no_marging">з ${tourAd.dateStart} по ${tourAd.dateEnd}</p>
                <p class="no_marging">${tourAd.country}, ${tourAd.city}</p>
                <p id="startDateP" class="hidden-p item_date-start">${tourAd.dateStart}</p>
                <p id="endDateP" class="hidden-p item_date-end">${tourAd.dateEnd}</p>
                <p id="countryNameP" class="hidden-p item_name-country">${tourAd.country}</p>
                <p id="cityNameP" class="hidden-p item_name-city">${tourAd.city}</p>
            </div>
            <div class="date-made-style color-box-tour-ad ">
                <p class="no_marging item_name-place number-of-people" style="padding-left: 5px;">
                    Кількість осіб для
                    максимальної
                    знижки <strong>${tourAd.discountSizePeople}</strong></p>
                <p id="discountSizePeopleP" class="hidden-p text-discount-size-people">${tourAd.discountSizePeople}</p>
            </div>
        </div>

    </div>

    <div id="rating-and-number-of-people" class="div-flex-column-base">
        <div class="tour-ad-box color-box-tour-ad">
            <p class="no_marging ">Місце перебування : <strong>${tourAd.place}</strong></p>


        </div>
        <p id="namePlaceP" class="hidden-p item_name-place">${tourAd.place}</p>
        <p id="ratingValueP" class="hidden-p item_rating-travel-agency">${tourAd.ratingAgency}</p>
        <div class="div-flex-column stars-style">
            ${getStart(tourAd.stars)}

        </div>
    </div>

    <div id="discount-and-date-made" class="div-flex-column-base">
        <div class="tour-ad-box color-box-tour-ad" style="align-items: center;">
            <p class="no_marging">Відсоткова знижка <strong>${tourAd.discountPercentage}</strong>%</p>
            <p id="discountPercentageP" class="hidden-p item_discount-percentage">${tourAd.discountPercentage}</p>
        </div>
        <div style="height: 18px;"></div>
        <div class="date-made-style tour-ad-box color-box-tour-ad">
            <h6 class="no_marging name-top">Дата утворення</h6>
            <p class="no_marging name-bottom item_date-new">${tourAd.dateRegistration.replace('T',' ')}</p>

        </div>
    </div>

    <div id="cost-one-customer" class="div-flex-column-base cost-block color-box-tour-ad">
        <h5>Ціна за 1 людину</h5>
        <p class="item_cost-for-customer">${tourAd.costOneCustomer}</p>
    </div>

    <div id="made-and-set-reting" class="div-flex-column-base">
        <a href="" class="made-order made-buttom">Оформити</a>
        <a href="" class="made-rating made-buttom">Оцінити</a>
    </div>
</div>`;
}
