const classDateNew = '.item_date-new';//1

const classNameTravelAgency = '.item_name-travel-agency';//1

const classRating0fTravelAgency = '.item_rating-travel-agency';//1

const classDateStart = '.item_date-start';//2

const classDateEnd = '.item_date-end';//3

const classTourAdForCustomerCost = '.item_cost-for-customer';//4

const classDiscountPercentage = '.item_discount-percentage';//5

const classDiscountSizePeople = '.text-discount-size-people';

const classCountry = '.item_name-country';

const classCity = '.item_name-city';

const classPlace = '.item_name-place';
// // -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+

function getMethodBySelectedName(selectedName){
    //console.log(selectedName);
    if(selectedName == 'sortByDateRegistration'){
      sortDivs(classDateNew,how_sort_string);
        return;
    }
    if(selectedName == 'sortByNameTravelAgency'){
      sortDivs(classNameTravelAgency,how_sort_string);
      return;
    }
    if(selectedName == 'sortByRatingOfTravelAgency'){
      sortDivs(classRating0fTravelAgency,how_sort_number);
      return;
    }
    if(selectedName == 'sortByDateStart'){
      sortDivs(classDateStart,how_sort_string);
      return;
    }
    if(selectedName == 'sortByDateEnd'){
      sortDivs(classDateEnd,how_sort_string);
      return;
    }
    if(selectedName == 'sortByTourAdForCustomerCost'){
      sortDivs(classTourAdForCustomerCost,how_sort_number);
      return;
    }
    if(selectedName == 'sortByDiscountPercentage'){
      sortDivs(classDiscountPercentage,how_sort_number);
      return;
    }
    if(selectedName == 'sortByDiscountSizePeople'){
      sortDivs(classDiscountSizePeople,how_sort_number);
      return;
    }
    if(selectedName == 'sortByCountry'){
      sortDivs(classCountry,how_sort_string);
      return;
    }
    if(selectedName == 'sortByCity'){
      sortDivs(classCity,how_sort_string);
        return;
    }
    if(selectedName == 'sortByPlace'){
      sortDivs(classPlace,how_sort_string);
        return;
    }
}

