let nameBox= null;//++
let STYLE_SORT_BUTTON= null;//++

// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+

var isChange = null;//++
var getMethodBySelectedName= null;//++
let sortDescendingBtn= null;//++
let sortAscendingBtn= null;//++
let btn= null;//++

function setElementsForSortSort(inputGetMethodBySelectedName,methodForCheckChange, inputNameBox,inputSTYLE_SORT_BUTTON,tegSortDescendingBtn,tegSortAscendingBtn,selecdIdTeg){
  getMethodBySelectedName = inputGetMethodBySelectedName;
  isChange = methodForCheckChange;
  nameBox = inputNameBox;
  STYLE_SORT_BUTTON = inputSTYLE_SORT_BUTTON;
  sortDescendingBtn = tegSortDescendingBtn;
  sortAscendingBtn = tegSortAscendingBtn;
  btn = selecdIdTeg;

  sortDescendingBtn.addEventListener('click', sortDescending);
  sortAscendingBtn.addEventListener('click', sortAscending);

  btn.onclick = (event) => {
    if(isChange()){
      event.preventDefault();
      getMethodBySelectedName(btn.options[btn.selectedIndex].id);
    }
  };
}

// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+


let how_sort_number = function(a,b) { return a-b};
let how_sort_string = function(a,b) {return (a > b) - (a < b)};

function sortDescending(){

  sortDescendingBtn.classList.add(STYLE_SORT_BUTTON);
  sortAscendingBtn.classList.remove(STYLE_SORT_BUTTON);

  how_sort_number = function(a,b) { return b-a};
  how_sort_string = function(a,b) {return (b > a) - (b < a)};

  getMethodBySelectedName(btn.options[btn.selectedIndex].id);
}

function sortAscending(){

  sortAscendingBtn.classList.add(STYLE_SORT_BUTTON);
  sortDescendingBtn.classList.remove(STYLE_SORT_BUTTON);

  how_sort_number = function(a,b) { return a-b};
  how_sort_string = function(a,b) {return (a > b) - (a < b)};

  getMethodBySelectedName(btn.options[btn.selectedIndex].id);
}



// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+




function sortDivs(fieldForSort,sortFunction){
  var items = document.querySelectorAll(nameBox);

  // get all items as an array and call the sort method
 
  Array.from(items).sort(function(a, b) {
    // get the text content
    a = a.querySelector(fieldForSort).innerText.toLowerCase()
    b = b.querySelector(fieldForSort).innerText.toLowerCase()
    return sortFunction(a,b);
  }).forEach(function(n, i) {
    n.style.order = i
  })
}

// -+-+---+-+-+-+-+-+--+-++--+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++--+-+-+-+-+-+-+
var valueOpinue;

function changeOption() {
    var selectedOption = btn.options[btn.selectedIndex];

    if (valueOpinue == selectedOption.text) {
        return false;
    }

    valueOpinue = selectedOption.text;
    return true;
}
