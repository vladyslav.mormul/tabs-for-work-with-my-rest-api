var countries = null;
async function getArrayFromRequest(url) {
    var arrayFrom = new Array();
    await fetch(url, {
      })
      .then((res) =>  res.json())
      .then((arrayField) => {
        arrayFrom= arrayFrom.concat(arrayField);
      })
    return arrayFrom;
}

var countriesInput = null;

async function setCountries(URL,WHERE_TO_INSTALL_ID) {
    if(countries == null){
        countries = await getArrayFromRequest(URL);
    }
    if(countriesInput == null){
        countriesInput = document.getElementById(WHERE_TO_INSTALL_ID);
    }
    countriesInput.innerHTML = getOption(countries);
}

function getOption(elements){
    var options = "";
    for (const element of elements) {
        options+= `<option value="${element}"></option>`;
    }
    return options;
}